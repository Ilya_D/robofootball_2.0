#include <AFMotor.h>
#include <SoftwareSerial.h>

SoftwareSerial mySerial(9, 2); // RX, TX
AF_DCMotor R_motor(3);
AF_DCMotor L_motor(4);
const int shovel = 10;
int SPEED = 200;
int BT_val; // значение приходящие с блютузмодуля
float K = 3; // коэффициент для подруливания

void setup()
{
  Serial.begin(9600);           // set up Serial library at 9600 bps
  mySerial.begin(115200);
  R_motor.run(RELEASE);
  L_motor.run(RELEASE);
  pinMode (shovel, OUTPUT);
}

void loop()
{
  /*BT_val = mySerial.read();
  if (BT_val == 'F') // движение вперед
  {
    Serial.println ("forard");
    R_motor.setSpeed(SPEED);
    L_motor.setSpeed(SPEED);
    R_motor.run(FORWARD);
    L_motor.run(FORWARD);
  }
  else if (BT_val == 'B')
  {
    Serial.println ("backward");
    R_motor.setSpeed(SPEED);
    L_motor.setSpeed(SPEED);
    R_motor.run(BACKWARD);
    L_motor.run(BACKWARD);
  }
  else if (BT_val == 'L')
  {
    Serial.println ("right");
    R_motor.setSpeed(SPEED);
    L_motor.setSpeed(SPEED);
    R_motor.run(BACKWARD);
    L_motor.run(FORWARD);
  }
  else if (BT_val == 'R')
  {
    Serial.println ("left");
    R_motor.setSpeed(SPEED);
    L_motor.setSpeed(SPEED);
    R_motor.run(FORWARD);
    L_motor.run(BACKWARD);
  }
  else if (BT_val == 'G')
  {
    Serial.println ("forward/right");
    R_motor.setSpeed(SPEED / K);
    L_motor.setSpeed(SPEED);
    R_motor.run(FORWARD);
    L_motor.run(FORWARD);
  }
  else if (BT_val == 'I')
  {
    Serial.println ("forward/left");
    R_motor.setSpeed(SPEED);
    L_motor.setSpeed(SPEED / K);
    R_motor.run(FORWARD);
    L_motor.run(FORWARD);
  }
  else if (BT_val == 'H')
  {
    Serial.println ("backward/left");
    R_motor.setSpeed(SPEED);
    L_motor.setSpeed(SPEED / K);
    R_motor.run(BACKWARD);
    L_motor.run(BACKWARD);
  }
  else if (BT_val == 'J')
  {
    Serial.println ("backward/right");
    R_motor.setSpeed(SPEED / K);
    L_motor.setSpeed(SPEED);
    R_motor.run(BACKWARD);
    L_motor.run(BACKWARD);
  }
  else if (BT_val == 'S')
  {
    Serial.println ("Stop");
    R_motor.run(RELEASE);
    L_motor.run(RELEASE);
  }
  else if (BT_val == 'x' || BT_val == 'X')
  {
    Serial.println ("удар");
    digitalWrite (shovel, HIGH);
    delay (60);
    digitalWrite (shovel, LOW);
  }
  
  
    R_motor.setSpeed(SPEED);
    L_motor.setSpeed(SPEED);
    R_motor.run(FORWARD);
    L_motor.run(FORWARD);*/
  if (mySerial.available())
  Serial.write(mySerial.read());
}
